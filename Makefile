all: build

prepush: build test spell lint

build:
	cargo build

run:
	cargo run

test:
	cargo test --workspace --verbose

lint:
	cargo clippy

spell:
	cspell -c ./.cspell.json "**"

clean:
	cargo clean
