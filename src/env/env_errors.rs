use std::{
    env::VarError,
    fmt::Display, io::Error,
};

#[derive(Debug)]
pub enum EnvErrors {
    VarError(VarError),
    IoError(std::io::Error),
}

impl Display for EnvErrors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EnvErrors::VarError(parse_int_error) => write!(f, "{}", parse_int_error),
            EnvErrors::IoError(io_error) => write!(f, "{}", io_error),
        }
    }
}

impl From<VarError> for EnvErrors {
    fn from(err: VarError) -> Self {
        EnvErrors::VarError(err)
    }
}

impl From<std::io::Error> for EnvErrors {
    fn from(err: std::io::Error) -> Self {
        EnvErrors::IoError(err)
    }
}

impl EnvErrors {
    pub fn new_io(message: &str) -> EnvErrors {
        return EnvErrors::IoError(Error::new(
            std::io::ErrorKind::Other,
            message
        ))
    }
}
