use std::{
    env::{self, VarError},
    fs::create_dir_all,
    path::{Path, PathBuf},
};

use super::env_errors::EnvErrors;

pub const WORKING_DIR: &str = "CLIMU_WORKING_DIRECTORY";
const IS_DETACHED: &str = "CLIMU_IS_DETACHED";
pub const DEFAULT_LOCATION: &str = ".local/share/climu";

fn default_work_dir() -> Result<PathBuf, VarError> {
    let home = env::var("HOME")?;
    let wd = Path::new(&home).join(DEFAULT_LOCATION);

    Ok(wd.to_path_buf())
}

pub fn init_work_dir(wd: Option<String>) -> Result<(), EnvErrors> {
    let work_dir: PathBuf = match wd {
        Some(p) => Path::new(&p).to_path_buf(),
        None => default_work_dir()?,
    };
    if !Path::new(&work_dir).is_dir() {
        create_dir_all(&work_dir)?;
    }
    env::set_var(WORKING_DIR, &work_dir);

    Ok(())
}

pub fn init_detached(is_detached: bool) {
    env::set_var(IS_DETACHED, is_detached.to_string());
}

pub fn get_working_dir() -> Result<String, EnvErrors> {
    Ok(env::var(WORKING_DIR)?)
}

pub fn is_detached() -> Result<bool, EnvErrors> {
    let detached_var = env::var(IS_DETACHED)?;
    match detached_var.as_str() {
        "true" => Ok(true),
        "false" => Ok(false),
        _ => Err(EnvErrors::new_io(&format!("Cannot read {} env variable: {}", IS_DETACHED, detached_var)))
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    #[test]
    fn test_default_work_dir() {
        let wd = default_work_dir();
        assert!(wd.is_ok());
    }

    #[test]
    fn test_init_work_dir() {
        let dir = "/tmp/climu_unittest/climu";
        let res = init_work_dir(Some(String::from(dir)));
        assert!(res.is_ok());
        assert!(Path::new(dir).is_dir());
        fs::remove_dir_all("/tmp/climu_unittest").unwrap();

        let env = env::var(WORKING_DIR).unwrap();
        assert_eq!(env, dir);
    }
}
