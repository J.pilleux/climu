use clap::{Args, Parser, Subcommand};

const DEFAULT_SIZE: &str = "30G";
const DEFAULT_MEM: i32 = 4096;
const DEFAULT_CPU: i32 = 4;

#[derive(Parser)]
#[clap(author, version, about)]
pub struct Cli {
    #[clap(short, long, value_name = "WORK_DIR")]
    pub work_dir: Option<String>,

    #[clap(short, long, default_value_t = true)]
    pub detached: bool,

    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Adds a new volume
    New(New),
    /// List all volumes
    List,
    /// Install a system into a given volume
    Install(Install),
    /// Boot a volume
    Boot(Boot),
    /// Remove a volume
    Remove(Remove),
    /// Backup a volume
    Backup(Backup),
    /// List all volume backups
    ListBackups(ListBackups),
    /// Restore a backup
    Restore(Restore),
    /// Remove a backup
    RemoveBackup(RemoveBackup),
    /// Tui
    Tui,
}

#[derive(Args)]
pub struct New {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
    #[clap(short, long, value_parser, value_name = "SIZE", default_value_t = String::from(DEFAULT_SIZE))]
    pub size: String,
}

#[derive(Args)]
pub struct Install {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
    #[clap(short, long, value_parser, value_name = "IMG")]
    pub image: String,
    #[clap(short, long, value_parser, value_name = "MEMORY", default_value_t = DEFAULT_MEM)]
    pub memory: i32,
    #[clap(short, long, value_parser, value_name = "CPU", default_value_t = DEFAULT_CPU)]
    pub cpu: i32,
}

#[derive(Args)]
pub struct Boot {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
    #[clap(short, long, value_parser, value_name = "MEMORY", default_value_t = DEFAULT_MEM)]
    pub memory: i32,
    #[clap(short, long, value_parser, value_name = "CPU", default_value_t = DEFAULT_CPU)]
    pub cpu: i32,
}

#[derive(Args)]
pub struct Remove {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
}

#[derive(Args)]
pub struct Backup {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
    #[clap(short, long, value_parser, value_name = "BACKUP_NAME")]
    pub backup_name: String,
}

#[derive(Args)]
pub struct ListBackups {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
}

#[derive(Args)]
pub struct Restore {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
    #[clap(short, long, value_parser, value_name = "BACKUP_NAME")]
    pub backup_name: String,
}

#[derive(Args)]
pub struct RemoveBackup {
    #[clap(value_parser, value_name = "VOL_NAME")]
    pub name: String,
    #[clap(short, long, value_parser, value_name = "BACKUP_NAME")]
    pub backup_name: String,
}

pub fn parse_args() -> Cli {
    Cli::parse()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Cli::command().debug_assert()
    }
}
