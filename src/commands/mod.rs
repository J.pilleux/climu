pub mod backup;
pub mod boot;
pub mod cmd_data;
mod common;
pub mod install;
pub mod list;
pub mod list_backups;
pub mod new;
pub mod remove;
pub mod remove_backup;
pub mod restore;
mod shell;

use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::env::env_errors::EnvErrors;

use self::{
    backup::backup_volume, boot::boot_volume, cmd_data::CmdData, install::install_volume,
    new::new_volume, remove::remove_volume, remove_backup::remove_backup, restore::restore,
};


#[derive(EnumIter)]
pub enum Commands {
    New,
    Install,
    Boot,
    Remove,
    Backup,
    Restore,
    RemoveBackup,
}

impl Commands {
    pub fn to_string(&self) -> String {
        match self {
            Commands::New => "New".to_string(),
            Commands::Install => "Install".to_string(),
            Commands::Boot => "Boot".to_string(),
            Commands::Remove => "Remove".to_string(),
            Commands::Backup => "Backup".to_string(),
            Commands::Restore => "Restore".to_string(),
            Commands::RemoveBackup => "Remove Backup".to_string(),
        }
    }

    pub fn from_string(cmd: String) -> Result<Commands, EnvErrors> {
        match cmd.as_str() {
            "New" => Ok(Commands::New),
            "Install" => Ok(Commands::Install),
            "Boot" => Ok(Commands::Boot),
            "Remove" => Ok(Commands::Remove),
            "Backup" => Ok(Commands::Backup),
            "Restore" => Ok(Commands::Restore),
            "Remove Backup" => Ok(Commands::RemoveBackup),
            _ => Err(EnvErrors::new_io(&format!("Cannot cast '{}' into a command", cmd)))
        }
    }

    pub fn get_all_commands() -> Vec<String> {
        let mut res: Vec<String> = Vec::new();
        for cmd in Commands::iter() {
            res.push(cmd.to_string());
        }

        res
    }

    pub fn need_input(&self) -> bool {
        match self {
            Commands::New | Commands::Backup => true,
            _ => false,
        }
    }

    pub fn need_backup_select(&self) -> bool {
        match self {
            Commands::Restore | Commands::RemoveBackup => true,
            _ => false,
        }
    }

    pub fn need_confirmation(&self) -> bool {
        match self {
            Commands::Remove | Commands::RemoveBackup => true,
            _ => false,
        }
    }

    pub fn run_command(data: &CmdData) -> String {
        let command = match Commands::from_string(String::from(&data.action)) {
            Ok(ret) => ret,
            Err(err) => return err.to_string(),
        };

        let cmd_ret = match command {
            Commands::New => new_volume(&data.user_input, &data.size),
            Commands::Install => install_volume(&data.vol_name, &data.iso_path, data.memory, data.cpu),
            Commands::Boot => boot_volume(&data.vol_name, data.memory, data.cpu),
            Commands::Remove => remove_volume(&data.vol_name),
            Commands::Backup => backup_volume(&data.vol_name, &data.user_input),
            Commands::Restore => restore(&data.vol_name, &data.backup_name),
            Commands::RemoveBackup => remove_backup(&data.vol_name, &data.backup_name),
        };

        match cmd_ret {
            Ok(ret) => ret,
            Err(err) => err.to_string(),
        }
    }
}
