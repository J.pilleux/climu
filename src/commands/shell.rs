use std::process::{Command, Stdio};

use crate::env::{env_errors::EnvErrors, env::is_detached};

use super::common::handle_output;

pub fn run_shell_command(cmd: &str, args: Vec<&str>, detached_msg: String) -> Result<String, EnvErrors> {
    let mut command = Command::new(cmd);
    command.args(args);
    let is_detached = is_detached()?;

    if is_detached {
        command.stdout(Stdio::null());
        command.stderr(Stdio::null());
        command.spawn()?;
        Ok(detached_msg)
    } else {
        let output = command.output()?;
        Ok(handle_output(output)?)
    }
}
