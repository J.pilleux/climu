use std::{env, path::PathBuf};

use crate::env::{env::WORKING_DIR, env_errors::EnvErrors};

use super::common::list_folder;

pub fn get_volumes_list() -> Result<Vec<String>, EnvErrors> {
    let work_dir = env::var(WORKING_DIR)?;

    Ok(list_folder(&work_dir, &|p: &PathBuf| p.is_dir())?)
}
