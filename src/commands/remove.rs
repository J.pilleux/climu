use std::fs::remove_dir_all;

use crate::env::env_errors::EnvErrors;

use super::common::get_volume_dir;

pub fn remove_volume(name: &str) -> Result<String, EnvErrors> {
    let volume_dir = get_volume_dir(name)?;
    remove_dir_all(volume_dir)?;

    Ok(format!("Volume {} removed", name))
}
