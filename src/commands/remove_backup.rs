use std::fs::remove_file;

use crate::env::env_errors::EnvErrors;

use super::common::get_volume_backup_file_path;

pub fn remove_backup(name: &str, backup_name: &str) -> Result<String, EnvErrors> {
    let backup_file_path = get_volume_backup_file_path(name, backup_name)?;

    remove_file(backup_file_path)?;
    Ok(format!("Backup {} of volume {} removed", backup_name, name))
}
