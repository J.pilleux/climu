use std::path::Path;

use crate::env::env_errors::EnvErrors;

use super::new::validate_size;

pub struct CmdData {
    pub vol_name: String,
    pub backup_name: String,
    pub action: String,
    pub user_input: String,
    pub cpu: i32,
    pub memory: i32,
    pub size: String,
    pub iso_path: String,
}

const VOLUME_STR: &str = "Volume name";
const BACKUP_STR: &str = "Backup name";
const ACTION_STR: &str = "Action";
const CPU_STR: &str = "CPU";
const MEM_STR: &str = "Memory";
const SIZE_STR: &str = "Size";
const ISO_STR: &str = "ISO path";

const DEFAULT_SIZE: &str = "30G";
const DEFAULT_RAM: i32 = 4;
const DEFAULT_CPU: i32 = 4;

impl CmdData {
    pub fn new(
        vol_name: String,
        backup_name: String,
        action: String,
        user_input: String,
    ) -> CmdData {
        CmdData {
            vol_name,
            backup_name,
            action,
            user_input,
            cpu: DEFAULT_CPU,
            memory: DEFAULT_RAM,
            size: DEFAULT_SIZE.to_string(),
            iso_path: String::from(""),
        }
    }

    pub fn default() -> CmdData {
        CmdData {
            vol_name: String::from(""),
            backup_name: String::from(""),
            action: String::from(""),
            user_input: String::from(""),
            cpu: DEFAULT_CPU,
            memory: DEFAULT_RAM,
            size: DEFAULT_SIZE.to_string(),
            iso_path: String::from(""),
        }
    }

    pub fn to_vec(&self) -> Vec<String> {
        let mut v = vec![
            format!("{}: {}", VOLUME_STR, self.vol_name),
            format!("{}: {}", BACKUP_STR, self.backup_name),
            format!("{}: {}", ACTION_STR, self.action),
        ];

        v.extend(self.get_editable_data_vec());
        v
    }

    pub fn get_editable_data_vec(&self) -> Vec<String> {
        vec![
            format!("{}: {}", CPU_STR, self.cpu.to_string()),
            format!("{}: {}", MEM_STR, self.memory.to_string()),
            format!("{}: {}", SIZE_STR, self.size),
            format!("{}: {}", ISO_STR, self.iso_path),
        ]
    }

    pub fn update_single_value(&mut self, full_entry: &str, input: &str) -> Result<(), EnvErrors> {
        let data_name: Vec<&str> = full_entry.split(":").collect();

        match data_name[0] {
            CPU_STR | MEM_STR => {
                let value: i32 = match input.parse() {
                    Ok(i) => i,
                    Err(_) => {
                        return Err(EnvErrors::new_io(&format!(
                            "Cannot cast '{}' into int",
                            input
                        )))
                    }
                };
                if data_name[0] == CPU_STR {
                    self.cpu = value;
                } else if data_name[0] == MEM_STR {
                    self.memory = value;
                }
            }
            SIZE_STR => {
                validate_size(input.to_string())?;
                self.size = input.to_string();
            }
            ISO_STR => {
                if !Path::new(input).is_file() {
                    return Err(EnvErrors::new_io(&format!(
                        "The file '{}' does not exists",
                        input
                    )));
                }
                self.iso_path = input.to_string();
            }
            _ => {
                return Err(EnvErrors::new_io(&format!(
                    "{} is not a valid action",
                    data_name[0]
                )));
            }
        }

        Ok(())
    }
}
