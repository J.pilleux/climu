use std::{
    env,
    fs::read_dir,
    path::{Path, PathBuf},
    process::Output,
};

use crate::env::{env::WORKING_DIR, env_errors::EnvErrors};

pub const BACKUP_FOLDER: &str = "backups";
pub const VOLUME_EXT: &str = "qcow";

fn check_path_exists(path: &str, vol_name: &str) -> Result<(), EnvErrors> {
    if !Path::new(&path).exists() {
        return Err(EnvErrors::new_io(&format!(
            "The volume {} does not exists",
            vol_name
        )));
    }

    Ok(())
}

pub fn get_volume_path(name: &str) -> Result<String, EnvErrors> {
    let work_dir = env::var(WORKING_DIR)?;
    let volume_path = format!("{}/{}/{}.{}", &work_dir, name, name, VOLUME_EXT);

    check_path_exists(&volume_path, name)?;

    Ok(volume_path)
}

pub fn get_volume_dir(name: &str) -> Result<String, EnvErrors> {
    let work_dir = env::var(WORKING_DIR)?;
    let volume_dir = format!("{}/{}", &work_dir, name);

    check_path_exists(&volume_dir, name)?;

    Ok(volume_dir)
}

pub fn get_volume_backup_dir(name: &str) -> Result<String, EnvErrors> {
    let work_dir = env::var(WORKING_DIR)?;
    let volume_dir = format!("{}/{}/{}", &work_dir, name, BACKUP_FOLDER);

    check_path_exists(&volume_dir, name)?;

    Ok(volume_dir)
}

pub fn get_volume_backup_file_path(
    volume_name: &str,
    backup_name: &str,
) -> Result<String, EnvErrors> {
    let work_dir = env::var(WORKING_DIR)?;
    let backup_file_path = format!(
        "{}/{}/{}/{}.{}",
        &work_dir, volume_name, BACKUP_FOLDER, backup_name, VOLUME_EXT
    );

    check_path_exists(&backup_file_path, volume_name)?;

    Ok(backup_file_path)
}

pub fn handle_output(output: Output) -> Result<String, EnvErrors> {
    if !output.status.success() {
        return Err(EnvErrors::new_io(&String::from_utf8_lossy(&output.stderr)));
    }

    Ok(String::from_utf8_lossy(&output.stdout).to_string())
}

pub fn list_folder(dir: &str, f: &dyn Fn(&PathBuf) -> bool) -> Result<Vec<String>, EnvErrors> {
    let paths = read_dir(dir)?;
    let mut dirs: Vec<String> = Vec::new();

    for path in paths {
        let p = path.unwrap().path();
        if f(&p) {
            let folder_name = match p.file_stem() {
                Some(folder) => folder,
                None => return Err(EnvErrors::new_io("Cannot get folder name")),
            };
            match folder_name.to_str() {
                Some(s) => dirs.push(String::from(s)),
                None => return Err(EnvErrors::new_io("I do not know")),
            };
        }
    }

    Ok(dirs)
}
