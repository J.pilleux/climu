use std::path::PathBuf;

use crate::env::env_errors::EnvErrors;

use super::common::{get_volume_backup_dir, list_folder};

pub fn list_volume_backups(name: &str) -> Result<Vec<String>, EnvErrors> {
    let backup_dir = get_volume_backup_dir(name)?;
    Ok(list_folder(&backup_dir, &|p: &PathBuf| p.is_file())?)
}
