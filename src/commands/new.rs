use regex::Regex;

use std::{
    env,
    fs::create_dir_all,
    io::{Error, ErrorKind},
    path::Path,
};

use crate::env::{env::WORKING_DIR, env_errors::EnvErrors};

use super::{common::BACKUP_FOLDER, shell::run_shell_command};

pub fn validate_size(size: String) -> Result<(), Error> {
    let re = match Regex::new(r"^\d+[GM]$") {
        Ok(r) => r,
        Err(err) => return Err(Error::new(ErrorKind::InvalidData, err)),
    };
    if !re.is_match(&size) {
        return Err(Error::new(
            ErrorKind::InvalidData,
            "The size does not match the right pattern: <INT>[MG]",
        ));
    }

    Ok(())
}

fn create_volume_folder(name: &str) -> Result<String, EnvErrors> {
    let work_dir = env::var(WORKING_DIR)?;
    let volume_folder = Path::new(&work_dir).join(&name);

    if Path::new(&volume_folder).is_dir() {
        return Err(EnvErrors::new_io(&format!(
            "The volume {:?} already exist",
            volume_folder
        )));
    } else {
        create_dir_all(&volume_folder)?;
        let backup_folder = Path::new(&volume_folder).join(BACKUP_FOLDER);
        create_dir_all(&backup_folder)?;
    }

    let volume_string = match volume_folder.to_str() {
        Some(s) => s,
        None => {
            return Err(EnvErrors::new_io(
                "Cannot convert volume folder into string",
            ))
        }
    };

    Ok(String::from(volume_string))
}

pub fn new_volume(name: &str, size: &str) -> Result<String, EnvErrors> {
    let volume_folder = create_volume_folder(name)?;
    let volume_full_path = format!("{}/{}.qcow", &volume_folder, &name);
    validate_size(String::from(size))?;

    let args: Vec<&str> = vec!["create", "-f", "qcow2", &volume_full_path, &size];
    Ok(run_shell_command(
        "qemu-img",
        args,
        format!("Creating new volume {}", name),
    )?)
}

#[test]
fn test_validate_size() {
    assert!(validate_size(String::from("10G")).is_ok());
    assert!(validate_size(String::from("10M")).is_ok());
    assert!(validate_size(String::from("M")).is_err());
    assert!(validate_size(String::from("")).is_err());
    assert!(validate_size(String::from("10000")).is_err());
    assert!(validate_size(String::from("-10G")).is_err());
    assert!(validate_size(String::from("10K")).is_err());
}
