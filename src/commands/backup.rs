use std::{fs::create_dir_all, path::Path};

use crate::env::env_errors::EnvErrors;

use super::{
    common::{get_volume_backup_dir, get_volume_path, VOLUME_EXT},
    shell::run_shell_command,
};

pub fn backup_volume(name: &str, backup_name: &str) -> Result<String, EnvErrors> {
    let backup_dir = get_volume_backup_dir(name)?;

    if !Path::new(&backup_dir).exists() {
        create_dir_all(&backup_dir)?;
    }

    let backup_destination = format!("{}/{}.{}", &backup_dir, backup_name, VOLUME_EXT);

    if Path::new(&backup_destination).exists() {
        return Err(EnvErrors::new_io(&format!(
            "Backup {} already exists",
            backup_destination
        )));
    }

    let volume_name = get_volume_path(name)?;

    let args = vec![
        "convert",
        "-c",
        &volume_name,
        "-O",
        "qcow",
        &backup_destination,
    ];
    Ok(run_shell_command(
        "qemu-img",
        args,
        format!("Backup volume {} into {}", name, backup_name),
    )?)
}
