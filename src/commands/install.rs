use std::{
    io::{Error, ErrorKind},
    path::Path,
};

use crate::env::env_errors::EnvErrors;

use super::{common::get_volume_path, shell::run_shell_command};

pub fn install_volume(name: &str, image: &str, memory: i32, cpu: i32) -> Result<String, EnvErrors> {
    let volume_path = get_volume_path(name)?;

    if !Path::new(&image).exists() {
        return Err(EnvErrors::IoError(Error::new(
            ErrorKind::InvalidData,
            format!("The image disk {} does not exists", image),
        )));
    }

    let str_memory = memory.to_string();
    let str_cpu = cpu.to_string();
    let args: Vec<&str> = vec![
        "-enable-kvm",
        "-hda",
        &volume_path,
        "-cdrom",
        image,
        "-boot",
        "d",
        "-m",
        &str_memory,
        "-smp",
        &str_cpu,
    ];

    Ok(run_shell_command(
        "qemu-system-x86_64",
        args,
        format!("Installing volume {} with image {}", name, image),
    )?)
}
