use crate::env::env_errors::EnvErrors;

use super::{common::get_volume_path, shell::run_shell_command};

pub fn boot_volume(name: &str, memory: i32, cpu: i32) -> Result<String, EnvErrors> {
    let volume_path = get_volume_path(name)?;

    let str_mem = memory.to_string();
    let str_cpu = cpu.to_string();

    let args: Vec<&str> = vec![
        "-enable-kvm",
        "-hda",
        &volume_path,
        "-boot",
        "d",
        "-m",
        &str_mem,
        "-smp",
        &str_cpu,
    ];

    Ok(run_shell_command(
        "qemu-system-x86_64",
        args,
        format!("Booting volume {}", name),
    )?)
}
