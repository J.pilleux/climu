use std::fs::{copy, remove_file};

use crate::env::env_errors::EnvErrors;

use super::common::{get_volume_backup_file_path, get_volume_path};

pub fn restore(name: &str, backup_name: &str) -> Result<String, EnvErrors> {
    let backup_path = get_volume_backup_file_path(name, backup_name)?;

    let volume_path = get_volume_path(name)?;
    remove_file(&volume_path)?;
    copy(&backup_path, &volume_path)?;

    Ok(format!("Backup {} restored", &backup_name))
}
