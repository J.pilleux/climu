use climu::args::{self, parse_args};
use inquire::Confirm;
use inquire::error::InquireResult;

use std::process::exit;

use climu::commands::{
    backup::backup_volume, boot::boot_volume, install::install_volume, list::get_volumes_list,
    list_backups::list_volume_backups, new::new_volume, remove::remove_volume,
    remove_backup::remove_backup, restore::restore,
};
use climu::env::env::{get_working_dir, init_detached, init_work_dir};
use climu::tui::main::run;

extern crate climu;

fn ask_remove_confirmation(name: &str) -> InquireResult<bool> {
    Confirm::new(&format!("Are you sure you want to remove {}?", name))
        .with_default(false)
        .with_help_message("This is cannot be reversed")
        .prompt()
}

fn print_list() {
    let work_dir = match get_working_dir() {
        Ok(wd) => wd,
        Err(err) => {
            eprint!("Cannot get working directory: {}", err);
            exit(1)
        }
    };
    println!("Working directory: {}", work_dir);
    println!("List of all available volumes:");
    match get_volumes_list() {
        Ok(list) => {
            if list.is_empty() {
                println!("No volume registered yet.");
            } else {
                for path in list {
                    println!("\t- {}", path);
                }
            }
        }
        Err(err) => eprintln!("Cannot list volumes: {:?}", err),
    }
}

fn main() {
    let args = parse_args();

    if let Err(err) = init_work_dir(args.work_dir) {
        eprintln!("Cannot initialize environment: {}", err)
    }
    init_detached(args.detached);

    match &args.command {
        args::Commands::New(new) => match new_volume(&new.name, &new.size) {
            Ok(ret) => println!("Volume created: {}", ret),
            Err(err) => eprintln!("Cannot create volume: {}", err),
        },
        args::Commands::List => print_list(),
        args::Commands::Install(install) => {
            match install_volume(&install.name, &install.image, install.memory, install.cpu) {
                Ok(ret) => println!("Volume installed: {}", ret),
                Err(err) => eprintln!("Cannot install volume: {}", err),
            }
        }
        args::Commands::Boot(boot) => match boot_volume(&boot.name, boot.memory, boot.cpu) {
            Ok(ret) => println!("Volume booted: {}", ret),
            Err(err) => eprintln!("Cannot boot volume: {}", err),
        },
        args::Commands::Remove(remove) => match ask_remove_confirmation(&remove.name) {
            Ok(true) => match remove_volume(&remove.name) {
                Ok(ret) => println!("{}", ret),
                Err(err) => eprintln!("{}", err),
            },
            Ok(false) => println!("Volume {} not removed", &remove.name),
            Err(err) => eprintln!("{}", err),
        },
        args::Commands::Backup(backup) => match backup_volume(&backup.name, &backup.backup_name) {
            Ok(ret) => println!("{}", ret),
            Err(err) => eprintln!("Cannot backup volume: {}", err),
        },
        args::Commands::ListBackups(list) => match list_volume_backups(&list.name) {
            Ok(ret) => {
                println!("List of backups for volume: {}", &list.name);
                if ret.is_empty() {
                    println!("No backups");
                } else {
                    for backup in ret {
                        println!("\t- {}", backup);
                    }
                }
            }
            Err(err) => eprintln!("Cannot list backups: {}", err),
        },
        args::Commands::Restore(rest) => match restore(&rest.name, &rest.backup_name) {
            Ok(ret) => println!("{}", ret),
            Err(err) => eprintln!("Cannot restore the backup: {}", err),
        },
        args::Commands::RemoveBackup(remove) => match ask_remove_confirmation(&remove.name) {
            Ok(true) => match remove_backup(&remove.name, &remove.backup_name) {
                Ok(ret) => println!("{}", ret),
                Err(err) => eprintln!("{}", err),
            },
            Ok(false) => println!(
                "Backup {} of volume {} not removed",
                &remove.backup_name, &remove.name
            ),
            Err(err) => eprintln!("{}", err),
        },
        args::Commands::Tui => match run() {
            Ok(_) => exit(0),
            Err(err) => eprintln!("Error during tui: {}", err),
        },
    };
}
