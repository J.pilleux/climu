use tui::{
    style::{Color, Modifier, Style},
    text::Spans,
    widgets::{Block, Borders, List, ListItem, Paragraph},
};

fn get_color(is_active: bool) -> Color {
    match is_active {
        true => Color::Green,
        false => Color::White,
    }
}

pub fn make_list<'a>(
    str_list: &'a Vec<String>,
    list_name: &'a str,
    is_active: bool,
) -> List<'a> {
    let list_items: Vec<ListItem> = str_list
        .iter()
        .map(|s| {
            let lines = vec![Spans::from(s.as_ref())];
            ListItem::new(lines).style(Style::default().fg(Color::White).bg(Color::Black))
        })
        .collect();


    List::new(list_items)
        .block(
            Block::default()
                .borders(Borders::ALL)
                .style(Style::default().fg(get_color(is_active)))
                .title(list_name),
        )
        .highlight_style(
            Style::default()
                .bg(Color::Magenta)
                .fg(Color::Black)
                .add_modifier(Modifier::BOLD),
        )
}

pub fn make_paragraph<'a>(prompt_input: &'a str, prompt_name: &'a str, is_active: bool) -> Paragraph<'a> {
    Paragraph::new(prompt_input)
        .style(Style::default().fg(get_color(is_active)))
        .block(Block::default().borders(Borders::ALL).title(prompt_name))
}

