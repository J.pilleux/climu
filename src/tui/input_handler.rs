use crossterm::event::KeyCode;
use std::{collections::HashMap, fmt::Display, process};

lazy_static! {
    static ref SPECIAL_KEYS: HashMap<&'static str, KeyCode> = {
        let mut key_map = HashMap::new();
        key_map.insert("enter", KeyCode::Enter);
        key_map.insert("backspace", KeyCode::Backspace);
        key_map.insert("up", KeyCode::Up);
        key_map.insert("down", KeyCode::Down);
        key_map.insert("left", KeyCode::Left);
        key_map.insert("right", KeyCode::Right);

        key_map
    };
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum NormalModeActions {
    Quit,
    Unselect,
    Next,
    Previous,
    Enter,
    Back,
    New,
    EditData,
}

pub struct KeyHandler {
    keys_map: HashMap<KeyCode, NormalModeActions>,
    key_description: String,
}

impl KeyHandler {
    pub fn new(
        quit: Vec<&str>,
        unselect: Vec<&str>,
        next: Vec<&str>,
        previous: Vec<&str>,
        enter: Vec<&str>,
        back: Vec<&str>,
        new: Vec<&str>,
        edit_data: Vec<&str>,
    ) -> Result<KeyHandler, String> {
        let mut key_handler = KeyHandler {
            keys_map: HashMap::new(),
            key_description: String::new(),
        };

        let mut duplicated_keys: Vec<String> = Vec::new();

        if let Err(mut dupes) = key_handler.map_keys_to_action(quit, NormalModeActions::Quit) {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) =
            key_handler.map_keys_to_action(unselect, NormalModeActions::Unselect)
        {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) = key_handler.map_keys_to_action(next, NormalModeActions::Next) {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) =
            key_handler.map_keys_to_action(previous, NormalModeActions::Previous)
        {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) = key_handler.map_keys_to_action(enter, NormalModeActions::Enter) {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) = key_handler.map_keys_to_action(back, NormalModeActions::Back) {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) = key_handler.map_keys_to_action(new, NormalModeActions::New) {
            duplicated_keys.append(&mut dupes);
        }
        if let Err(mut dupes) =
            key_handler.map_keys_to_action(edit_data, NormalModeActions::EditData)
        {
            duplicated_keys.append(&mut dupes);
        }

        if !duplicated_keys.is_empty() {
            let keys_str = duplicated_keys.join(", ");
            Err(format!("Duplicated keys detected : {}", keys_str))
        } else {
            key_handler.key_description = format!("{}", key_handler);
            Ok(key_handler)
        }
    }

    pub fn default() -> Result<KeyHandler, String> {
        KeyHandler::new(
            vec!["q"],
            vec!["Left"],
            vec!["t", "Down"],
            vec!["s", "Up"],
            vec!["r", "Enter"],
            vec!["Backspace", "c", "q"],
            vec!["n"],
            vec!["e"],
        )
    }

    #[allow(dead_code)]
    pub fn get_key_description(&self) -> &String {
        &self.key_description
    }

    fn map_keys_to_action(
        &mut self,
        keys: Vec<&str>,
        action: NormalModeActions,
    ) -> Result<(), Vec<String>> {
        let mut duplicated_keys = Vec::new();

        for key in KeyHandler::vec_string_to_vec_key_code(keys.to_vec()) {
            if self.keys_map.contains_key(&key) {
                duplicated_keys.push(format!("{:?}", key));
            }
            self.keys_map.insert(key, action);
        }

        if !duplicated_keys.is_empty() {
            Err(duplicated_keys)
        } else {
            Ok(())
        }
    }

    fn vec_string_to_vec_key_code(str_keys: Vec<&str>) -> Vec<KeyCode> {
        str_keys
            .into_iter()
            .map(KeyHandler::string_to_key_code)
            .collect()
    }

    fn key_code_to_string(key_code: KeyCode) -> String {
        let raw_str = format!("{:?}", key_code);
        if raw_str.starts_with("Char('") {
            let split: Vec<String> = raw_str.split("'").map(String::from).collect();
            String::from(&split[1])
        } else {
            raw_str
        }
    }

    fn string_to_key_code(str_key: &str) -> KeyCode {
        let key = str_key.to_lowercase();
        match SPECIAL_KEYS.get(&*key) {
            Some(key_code) => key_code.to_owned(),
            None => {
                let chars: Vec<char> = key.chars().collect();
                if chars.len() != 1 {
                    eprint!("Invalid key : {}", key);
                    process::exit(1)
                }
                KeyCode::Char(chars[0])
            }
        }
    }

    pub fn convert_to_action(&self, key_code: KeyCode) -> Option<NormalModeActions> {
        match self.keys_map.get(&key_code) {
            Some(code) => Some(code.clone()),
            None => None,
        }
    }
}

impl Display for KeyHandler {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Regrouping the keys by "action"
        let mut key_map: HashMap<String, Vec<String>> = HashMap::new();
        for (key, action) in self.keys_map.iter() {
            let actions = key_map
                .entry(format!("{:?}", &action))
                .or_insert(Vec::new());
            actions.push(KeyHandler::key_code_to_string(*key));
        }

        // Creating a list of each key which will be joined
        let mut map: Vec<String> = Vec::new();
        for (action, keys) in key_map.iter() {
            let entry = format!("{: <8} : {}", action, keys.join(", "));
            map.push(entry);
        }

        let map_description = map.join("\n");
        write!(f, "{}", map_description)
    }
}

#[cfg(test)]
mod tests {
    use crossterm::event::KeyCode;

    use crate::tui::input_handler::NormalModeActions;

    use super::KeyHandler;

    #[test]
    fn test_string_to_key_code() {
        // Normal case
        let actual = KeyHandler::string_to_key_code("a");
        let expected = KeyCode::Char('a');
        assert_eq!(actual, expected);

        // unicode test 1
        let actual = KeyHandler::string_to_key_code("é");
        let expected = KeyCode::Char('é');
        assert_eq!(actual, expected);

        // unicode test 2
        let actual = KeyHandler::string_to_key_code("«");
        let expected = KeyCode::Char('«');
        assert_eq!(actual, expected);

        // special keys
        let actual = KeyHandler::string_to_key_code("Enter");
        let expected = KeyCode::Enter;
        assert_eq!(actual, expected);

        let actual = KeyHandler::string_to_key_code("Backspace");
        let expected = KeyCode::Backspace;
        assert_eq!(actual, expected);

        let actual = KeyHandler::string_to_key_code("Up");
        let expected = KeyCode::Up;
        assert_eq!(actual, expected);

        let actual = KeyHandler::string_to_key_code("Down");
        let expected = KeyCode::Down;
        assert_eq!(actual, expected);

        let actual = KeyHandler::string_to_key_code("Left");
        let expected = KeyCode::Left;
        assert_eq!(actual, expected);

        let actual = KeyHandler::string_to_key_code("Right");
        let expected = KeyCode::Right;
        assert_eq!(actual, expected);

        // Special keys case unsensitive 1
        let actual = KeyHandler::string_to_key_code("ENTER");
        let expected = KeyCode::Enter;
        assert_eq!(actual, expected);

        // Special keys case unsensitive 2
        let actual = KeyHandler::string_to_key_code("enTEr");
        let expected = KeyCode::Enter;
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_normal_key_handler() {
        let key_handler = KeyHandler::new(
            vec!["q"],
            vec!["Left"],
            vec!["t", "Down"],
            vec!["s", "Up"],
            vec!["r", "Enter"],
            vec!["Backspace", "c"],
            vec!["n"],
            vec!["e"],
        );

        assert!(key_handler.is_ok());

        let handler = match key_handler {
            Ok(handler) => handler,
            Err(_) => panic!("should not happen"),
        };

        let actual_opt = handler.convert_to_action(KeyCode::Enter);
        let expected = NormalModeActions::Enter;

        match actual_opt {
            Some(actual) => assert_eq!(actual, expected),
            None => {}
        }

        let actual_opt = handler.convert_to_action(KeyCode::Char('q'));
        let expected = NormalModeActions::Quit;

        match actual_opt {
            Some(actual) => assert_eq!(actual, expected),
            None => {}
        }

        let actual_opt = handler.convert_to_action(KeyCode::Char('z'));

        assert!(actual_opt.is_none());
    }

    #[test]
    fn test_normal_key_handler_double_key_init() {
        let key_handler = KeyHandler::default();

        assert!(key_handler.is_err());
    }
}
