use crate::{
    commands::{
        cmd_data::CmdData, list::get_volumes_list, list_backups::list_volume_backups, Commands,
    },
    env::env_errors::EnvErrors,
};

use super::{
    input_field::InputField,
    input_handler::{KeyHandler, NormalModeActions},
    renderer::Renderer,
    stateful_list::StatefulList,
};
use crossterm::event::KeyCode;
use tui::{backend::Backend, Frame};

pub enum AppState {
    VolumeSelect,
    ActionSelect,
    BackupSelect,
    InputField,
    EditDataInputField,
    AskConfirmation,
    EditData,
}

enum Mode {
    Normal,
    Insert,
}

pub struct App {
    volume_names: StatefulList<String>,
    action_names: StatefulList<String>,
    backup_names: StatefulList<String>,
    ask_confirm: StatefulList<String>,
    command_data_list: StatefulList<String>,

    key_handler: KeyHandler,
    mode: Mode,

    input_field: InputField,
    text_display: String,
    cmd_data: CmdData,

    state: AppState,
}

impl App {
    pub fn new() -> Result<App, EnvErrors> {
        let volume_names = get_volumes_list()?;

        let key_handler = KeyHandler::default();

        let handler = match key_handler {
            Ok(handler) => handler,
            Err(err) => return Err(EnvErrors::new_io(&err)),
        };

        let commands = Commands::get_all_commands();

        let app = App {
            volume_names: StatefulList::with_items(volume_names),
            action_names: StatefulList::with_items(commands),
            backup_names: StatefulList::with_items(Vec::new()),
            ask_confirm: StatefulList::with_items(vec![
                String::from("Yes"),
                String::from("No"),
                String::from("Back"),
            ]),
            command_data_list: StatefulList::with_items(Vec::new()),
            key_handler: handler,
            mode: Mode::Normal,
            input_field: InputField::new(),
            text_display: String::new(),
            cmd_data: CmdData::default(),
            state: AppState::VolumeSelect,
        };

        Ok(app)
    }

    pub fn render<B: Backend>(&mut self, frame: &mut Frame<B>) {
        let mut renderer = Renderer::new(frame.size());
        renderer.render_title(frame, "Qemu volume manager");
        match self.state {
            AppState::AskConfirmation => {
                renderer.render_ask_confirmation(
                    frame,
                    self.ask_confirm.items.to_vec(),
                    &mut self.ask_confirm.state,
                );
            }
            AppState::InputField | AppState::EditDataInputField => {
                renderer.render_input_field(frame, self.input_field.get_value())
            }
            AppState::EditData => renderer.render_command_data_list(
                frame,
                self.command_data_list.items.to_vec(),
                &mut self.command_data_list.state,
                &self.text_display,
            ),
            _ => {
                renderer.render_volume_names(
                    frame,
                    self.volume_names.items.to_vec(),
                    &mut self.volume_names.state,
                    &self.state,
                );
                renderer.render_action(
                    frame,
                    self.action_names.items.to_vec(),
                    &mut self.action_names.state,
                    &self.state,
                );
                renderer.render_backups(
                    frame,
                    self.backup_names.items.to_vec(),
                    &mut self.backup_names.state,
                    &self.state,
                );
                renderer.render_display(frame, &self.text_display);
                renderer.render_cmd_data(frame, &self.cmd_data)
            }
        }
    }

    pub fn handle_key(&mut self, key_code: KeyCode) -> bool {
        let mut should_quit = false;
        match self.mode {
            Mode::Normal => {
                match self.key_handler.convert_to_action(key_code) {
                    Some(action) => match action {
                        NormalModeActions::Quit => should_quit = true,
                        NormalModeActions::Unselect => self.unselect(),
                        NormalModeActions::Next => self.next_item(),
                        NormalModeActions::Previous => self.previous_item(),
                        NormalModeActions::Enter => self.enter(),
                        NormalModeActions::Back => self.back(),
                        NormalModeActions::New => {
                            self.cmd_data.action = String::from("New");
                            self.goto_insert();
                        }
                        NormalModeActions::EditData => {
                            self.refresh_cmd_data_list();
                            self.state = AppState::EditData;
                        }
                    },
                    None => {}
                };
                self.refresh_cmd_data();
            }
            Mode::Insert => match key_code {
                KeyCode::Enter => {
                    self.cmd_data.user_input = self.input_field.get_value();
                    if matches!(self.state, AppState::InputField) {
                        self.text_display = self.run_command();
                        self.goto_normal();
                        self.update_current_volumes();
                    } else if matches!(self.state, AppState::EditDataInputField) {
                        self.change_data();
                        self.refresh_cmd_data_list();
                        self.input_field.reset();
                        self.mode = Mode::Normal;
                        self.state = AppState::EditData;
                    }
                }
                KeyCode::Char(c) => self.input_field.type_char(c),
                KeyCode::Backspace => self.input_field.erase(),
                KeyCode::Esc => {
                    if matches!(self.state, AppState::InputField) {
                        self.goto_normal();
                    } else if matches!(self.state, AppState::EditDataInputField) {
                        self.state = AppState::EditData;
                    }
                }
                _ => {}
            },
        }

        should_quit
    }

    pub fn unselect(&mut self) {
        match self.state {
            AppState::VolumeSelect => self.volume_names.unselect(),
            AppState::ActionSelect => self.action_names.unselect(),
            AppState::BackupSelect => self.backup_names.unselect(),
            AppState::InputField | AppState::EditDataInputField => {}
            AppState::AskConfirmation => self.ask_confirm.unselect(),
            AppState::EditData => self.command_data_list.unselect(),
        }
    }

    pub fn next_item(&mut self) {
        match self.state {
            AppState::VolumeSelect => {
                self.volume_names.next();
                self.update_current_backups();
            }
            AppState::ActionSelect => self.action_names.next(),
            AppState::BackupSelect => self.backup_names.next(),
            AppState::InputField | AppState::EditDataInputField => {}
            AppState::AskConfirmation => self.ask_confirm.next(),
            AppState::EditData => self.command_data_list.next(),
        }
    }

    pub fn previous_item(&mut self) {
        match self.state {
            AppState::VolumeSelect => {
                self.volume_names.previous();
                self.update_current_backups();
            }
            AppState::ActionSelect => self.action_names.previous(),
            AppState::BackupSelect => self.backup_names.previous(),
            AppState::InputField | AppState::EditDataInputField => {}
            AppState::AskConfirmation => self.ask_confirm.previous(),
            AppState::EditData => self.command_data_list.previous(),
        }
    }

    pub fn enter(&mut self) {
        match self.state {
            AppState::VolumeSelect => self.state = AppState::ActionSelect,
            AppState::ActionSelect => match self.get_current_action() {
                Ok(current_action) => {
                    if current_action.need_input() {
                        self.goto_insert();
                    } else if current_action.need_backup_select() {
                        self.state = AppState::BackupSelect;
                    } else if current_action.need_confirmation() {
                        self.state = AppState::AskConfirmation;
                    } else {
                        self.text_display = self.run_command();
                        self.update_current_volumes();
                    }
                }
                Err(err) => self.text_display = err.to_string(),
            },
            AppState::BackupSelect => {}
            AppState::InputField | AppState::EditDataInputField => {}
            AppState::AskConfirmation => match self.ask_confirm.get_current_item() {
                Some(answer) => match answer.to_lowercase().as_str() {
                    "yes" => {
                        self.state = AppState::VolumeSelect;
                        self.run_command();
                        self.update_current_volumes();
                        self.update_current_backups();
                    }
                    _ => self.goto_normal(),
                },
                None => self.text_display = String::from("Please select an answer"),
            },
            AppState::EditData => {
                self.state = AppState::EditDataInputField;
                self.mode = Mode::Insert;
            }
        }
    }

    pub fn back(&mut self) {
        match self.state {
            AppState::VolumeSelect => {}
            AppState::ActionSelect => self.state = AppState::VolumeSelect,
            AppState::BackupSelect => self.state = AppState::ActionSelect,
            AppState::InputField => {}
            AppState::EditDataInputField => {
                self.refresh_cmd_data();
                self.state = AppState::VolumeSelect;
            }
            AppState::AskConfirmation => self.goto_normal(),
            AppState::EditData => self.goto_normal(),
        }
    }

    fn refresh_cmd_data(&mut self) {
        if matches!(self.state, AppState::InputField)
            || matches!(self.state, AppState::EditDataInputField)
        {
            return;
        }
        let volume_name = match self.volume_names.get_current_item() {
            Some(name) => name.to_string(),
            None => String::from("No volume selected"),
        };
        self.cmd_data.vol_name = volume_name;

        let backup_name = match self.backup_names.get_current_item() {
            Some(backup) => backup.to_string(),
            None => String::from("No backup selected"),
        };
        self.cmd_data.backup_name = backup_name;

        let action = match self.action_names.get_current_item() {
            Some(action) => action.to_string(),
            None => String::from("No action selected"),
        };
        self.cmd_data.action = action;

        let user_input = self.input_field.get_value();
        self.cmd_data.user_input = user_input;
    }

    fn refresh_cmd_data_list(&mut self) {
        self.refresh_cmd_data();

        let data = self.cmd_data.get_editable_data_vec();
        self.command_data_list = StatefulList::with_items(data);
    }

    fn get_current_action(&mut self) -> Result<Commands, EnvErrors> {
        match &self.action_names.get_current_item() {
            Some(action) => Ok(Commands::from_string(action.to_string())?),
            None => Err(EnvErrors::new_io("Cannot get current action")),
        }
    }

    fn update_current_backups(&mut self) {
        let current_backups = self.get_current_backups();
        self.backup_names = StatefulList::with_items(current_backups);
    }

    fn update_current_volumes(&mut self) {
        let volume_names = get_volumes_list();

        match volume_names {
            Ok(names) => self.volume_names = StatefulList::with_items(names),
            Err(err) => self.text_display = err.to_string(),
        }
    }

    fn get_current_backups(&mut self) -> Vec<String> {
        match self.volume_names.get_current_item() {
            Some(name) => match list_volume_backups(name) {
                Ok(backups) => backups,
                Err(_) => Vec::new(),
            },
            None => Vec::new(),
        }
    }

    fn goto_normal(&mut self) {
        self.input_field.reset();
        self.mode = Mode::Normal;
        self.state = AppState::VolumeSelect;
    }

    fn goto_insert(&mut self) {
        self.mode = Mode::Insert;
        self.state = AppState::InputField;
    }

    fn unselect_all(&mut self) {
        self.ask_confirm.unselect();
        self.volume_names.unselect();
        self.backup_names.unselect();
        self.action_names.unselect();
    }

    fn run_command(&mut self) -> String {
        self.unselect_all();
        Commands::run_command(&self.cmd_data)
    }

    fn change_data(&mut self) {
        let current_data = match self.command_data_list.get_current_item() {
            Some(data) => data,
            None => {
                self.text_display = String::from("Cannot change data");
                return;
            }
        };
        match self
            .cmd_data
            .update_single_value(current_data, &self.input_field.get_value())
        {
            Ok(_) => {}
            Err(err) => self.text_display = err.to_string(),
        }
    }
}
