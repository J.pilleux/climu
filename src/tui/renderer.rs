use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    widgets::{Block, Borders, ListState, Paragraph},
    Frame,
};

use crate::commands::cmd_data::CmdData;

use super::{
    app::AppState,
    common::{make_list, make_paragraph},
};

pub struct Renderer {
    title: Rect,
    volumes: Rect,
    actions: Rect,
    backups: Rect,
    data: Rect,
    display: Rect,
}

impl Renderer {
    pub fn new(size: Rect) -> Renderer {
        let chunks = Layout::default()
            .direction(tui::layout::Direction::Vertical)
            .margin(1)
            .constraints(
                [
                    Constraint::Percentage(5),
                    Constraint::Percentage(80),
                    Constraint::Percentage(15),
                ]
                .as_ref(),
            )
            .split(size);

        let chunks_1 = Layout::default()
            .direction(tui::layout::Direction::Horizontal)
            .constraints(
                [
                    Constraint::Percentage(10),
                    Constraint::Percentage(10),
                    Constraint::Percentage(10),
                    Constraint::Percentage(70),
                ]
                .as_ref(),
            )
            .split(chunks[1]);

        Renderer {
            title: chunks[0],
            volumes: chunks_1[0],
            actions: chunks_1[1],
            backups: chunks_1[2],
            data: chunks[2],
            display: chunks_1[3],
        }
    }

    pub fn render_title<B: Backend>(&mut self, frame: &mut Frame<B>, title: &str) {
        let block = Block::default().borders(Borders::BOTTOM);

        let title_paragraph = Paragraph::new(title)
            .block(block)
            .alignment(Alignment::Center);

        frame.render_widget(title_paragraph, self.title);
    }

    pub fn render_volume_names<B: Backend>(
        &mut self,
        frame: &mut Frame<B>,
        volume_names: Vec<String>,
        volume_name_list_state: &mut ListState,
        state: &AppState,
    ) {
        let is_active = matches!(state, AppState::VolumeSelect);
        let volume_list = make_list(&volume_names, "Volumes", is_active);
        frame.render_stateful_widget(volume_list, self.volumes, volume_name_list_state)
    }

    pub fn render_action<B: Backend>(
        &mut self,
        frame: &mut Frame<B>,
        action_names: Vec<String>,
        action_name_list_state: &mut ListState,
        state: &AppState,
    ) {
        let is_active = matches!(state, AppState::ActionSelect);
        let action_list = make_list(&action_names, "Actions", is_active);
        frame.render_stateful_widget(action_list, self.actions, action_name_list_state);
    }

    pub fn render_backups<B: Backend>(
        &mut self,
        frame: &mut Frame<B>,
        backup_names: Vec<String>,
        backup_name_list_state: &mut ListState,
        state: &AppState,
    ) {
        let is_active = matches!(state, AppState::BackupSelect);
        let action_list = make_list(&backup_names, "Backups", is_active);
        frame.render_stateful_widget(action_list, self.backups, backup_name_list_state);
    }

    pub fn render_input_field<B: Backend>(&mut self, frame: &mut Frame<B>, input_value: String) {
        let chunk = Layout::default()
            .direction(tui::layout::Direction::Vertical)
            .constraints([
                Constraint::Percentage(10),
                Constraint::Percentage(42),
                Constraint::Percentage(5),
                Constraint::Percentage(43),
            ])
            .margin(1)
            .split(frame.size());

        let block = Block::default().borders(Borders::BOTTOM);
        let title_paragraph = Paragraph::new("Qemu volume manager")
            .block(block)
            .alignment(Alignment::Center);
        frame.render_widget(title_paragraph, chunk[0]);

        let field_layout = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([
                Constraint::Percentage(20),
                Constraint::Percentage(60),
                Constraint::Percentage(20),
            ])
            .split(chunk[2]);

        let input_field = make_paragraph(&input_value, "Input field", false);
        frame.render_widget(input_field, field_layout[1]);
        frame.set_cursor(
            field_layout[1].x + input_value.len() as u16 + 1,
            field_layout[1].y + 1,
        );
    }

    pub fn render_display<B: Backend>(&mut self, frame: &mut Frame<B>, display_value: &str) {
        let display = make_paragraph(&display_value, "Command return", false);
        frame.render_widget(display, self.display)
    }

    pub fn render_ask_confirmation<B: Backend>(
        &self,
        frame: &mut Frame<B>,
        answers: Vec<String>,
        answers_state: &mut ListState,
    ) {
        let chunk = Layout::default()
            .direction(tui::layout::Direction::Vertical)
            .constraints([Constraint::Percentage(100)])
            .margin(30)
            .split(frame.size());

        let action_list = make_list(&answers, "Are you sure ?", false);
        frame.render_stateful_widget(action_list, chunk[0], answers_state);
    }

    pub fn render_cmd_data<B: Backend>(&self, frame: &mut Frame<B>, cmd_data: &CmdData) {
        let data = cmd_data.to_vec();
        let data_list = make_list(&data, "Command data", false);
        frame.render_widget(data_list, self.data);
    }

    pub fn render_command_data_list<B: Backend>(
        &self,
        frame: &mut Frame<B>,
        cmd_data: Vec<String>,
        list_state: &mut ListState,
        edit_errors: &str
    ) {
        let chunk = Layout::default()
            .direction(tui::layout::Direction::Vertical)
            .constraints([
                Constraint::Percentage(30),
                Constraint::Percentage(40),
                Constraint::Percentage(10),
                Constraint::Percentage(20),
            ])
            .margin(1)
            .split(frame.size());

        let data_list = make_list(&cmd_data, "Command data edition", false);
        frame.render_stateful_widget(data_list, chunk[1], list_state);

        let errors_paragraph = make_paragraph(&edit_errors, "Edition errors", false);
        frame.render_widget(errors_paragraph, chunk[2]);
    }
}
