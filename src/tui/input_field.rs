pub struct InputField {
    field: String,
}

impl InputField {
    pub fn new() -> InputField {
        InputField {
            field: String::new(),
        }
    }

    pub fn type_char(&mut self, c: char) {
        self.field.push(c);
    }

    pub fn erase(&mut self) {
        self.field.pop();
    }

    pub fn get_value(&self) -> String {
        String::from(&self.field)
    }

    pub fn reset(&mut self) {
        self.field = String::new();
    }
}

